(function () {

    var card = {
        price:0,
        total: 0,
        products: []
    };

    var MAX_PROD_COUNT = 245;
    var PROD_COUNT_PER_LAP = 10;
    var STRS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    function make(length) {
        var result = '';
        var characters = STRS;
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    };

    function makeInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    function getRandomProduct() {
        return {
            name: make(15),
            description: make(40),
            price: makeInt(15, 450),
            id:card.total
        };
    };

    function createProductRow(product) {
        var html = '<div class="row satir">';
        html += '<div class="col-4"></div>';

        html += '\
        <div class="col-4 prod">\
            <div class="card" id="a' + product.id + '">\
                <div class="card-body">\
                    <div class="card-title"> Name : ' + product.name + '</div>\
                    <div class="card-text" style="font-weight: normal;font-size: 15px;margin-bottom: 19px;border-bottom: 1px dashed #333;padding-bottom: 10px;"> Desc:  ' + product.description + '</div>\
                    <div class="card-subtitle mb-2 text-muted"> Price :  ' + product.price + ' TL</div>\
                    <div class="btn btn-primary addToCart" id="' + product.id + '"  style="cursor:pointer;"> Sepete ekle </div>\
                </div>\
            </div>\
        </div>\
        ';

        html += '<div class="col-4"></div>';
        html += '</div>';
        return html;
    };

    function appendToBody(html) {
        jQuery('#contentOfProds').append(html);
    };

    function constructNewProductGroupModels() {
        var html = '';
        for (var i = 0; i < PROD_COUNT_PER_LAP; i++) {
            card.products.push(getRandomProduct());
            html += createProductRow(card.products[card.total]);
            card.total++;
        }
        appendToBody(html);
    };

    $("#returnTop").hide();
    constructNewProductGroupModels();
    
    

    window.onscroll = function() {
        var scrollHeight , totalHeight;
        scrollHeight = document.body.scrollHeight;
        totalHeight = window.innerHeight + window.scrollY;
        if(totalHeight >= scrollHeight + 50){
            jQuery(".btn-warning").show();
            constructNewProductGroupModels();
            
        }
    };

    

    $(document).on('click', '#refresh', function () {
        // Page Reload değil. Ürünlerin hepsini sıfırlayıp, yeniden random 10 ürün ekleyen fonk fakat sayfa kesinlikle yenilenmeden yapsın.
        jQuery(".satir").remove();
         card.total=0;
         card.products.length=0;
        constructNewProductGroupModels();
        
    });



    $(document).on('click', '#returnTop', function () {

        //fonk doğru fakat eleman sayfanın en altında sabit değil. aşağıya yakın ve bizimle birlikte hareket etmesi gerekiyor.

        document.documentElement.scrollTop = 0;
        $("#returnTop").hide();
    });



    $(document).on('click', '.addToCart', function () {
        card.price += card.products[this.id].price;
        jQuery('#total').html(card.price + ".00 TL");
        alert("Ürününüz sepete eklenmiştir.");
    });

    $(document).on('click', '.btn-danger', function () {
        jQuery('#total').html("0.00 TL");
        card.price=0;
    });

    $(document).on('keyup change', '#search', function(){
      var txt = $('#search').val();
      if(txt.length >= 3 ){
        $('.satir').hide();
        $(":contains('"+txt+"')").show();        
      }else{
        $('.satir').show();
      }
        
      
    })
    

})();
